# K8s crash course

## Cluster setup

Cf https://gitlab.com/jailbreak/infrastructure/kubernetes-cluster-management

## Notes

pods

- hello-1 label: app:hello
- hello-2 label: app:hello
- hello-3 label: app:hello
- dbnomics-ui-1 label: app:dbnomics-ui
- dbnomics-ui-2 label: app:dbnomics-ui

clé unique des objects k8s (namespace, kind, name)
